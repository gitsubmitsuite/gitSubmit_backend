var events = require('./events'),
    func = events('pull', require('../pull'));

func.placeholder = require('./placeholder');
module.exports = func;
